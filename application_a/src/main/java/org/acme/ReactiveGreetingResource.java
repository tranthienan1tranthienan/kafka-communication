package org.acme;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class ReactiveGreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello RESTEasy Reactive";
    }

    @Inject
    @Channel("source-out")
    Emitter<String> stringEmitter;

    @GET
    @Path("/{string}")
    public String printString(@PathParam("string") String stringToPrint){
        stringEmitter.send(stringToPrint);
        return stringToPrint+" emitted";
    }


}